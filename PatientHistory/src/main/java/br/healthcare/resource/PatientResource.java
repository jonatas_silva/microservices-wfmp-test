package br.healthcare.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.healthcare.model.BloodPressure;
import br.healthcare.model.HealthNotification;
import br.healthcare.model.MedicalReport;

@RestController
@RequestMapping("/patient/history")
public class PatientResource {
	
	private static final Logger log = LoggerFactory.getLogger(PatientResource.class);
	
	@RequestMapping(value = "/notification", method = RequestMethod.POST)
	public @ResponseBody String history(@RequestBody HealthNotification healthNotification){
		System.out.println("Registered.\n"+healthNotification.toString());
		return "";
	}
	
	@RequestMapping(value = "/medical/report", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody MedicalReport save(@RequestBody MedicalReport report){
		System.out.println("Registered.\n"+report.toString());
		report.setDescription(report.getDescription().toUpperCase());
		return report;
	}
	
	@RequestMapping(value = "/bloodpressure", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody BloodPressure save(@RequestBody BloodPressure bloodPressure){
		System.out.println("Registered.\n"+bloodPressure.toString());
		return bloodPressure;
	}
}
