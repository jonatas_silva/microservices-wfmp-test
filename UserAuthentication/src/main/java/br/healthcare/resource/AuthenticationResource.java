package br.healthcare.resource;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.healthcare.model.User;
import br.healthcare.service.AuthenticationService;

@RestController
@RequestMapping("/wfmp")
public class AuthenticationResource {
	
	private static final Logger log = LoggerFactory.getLogger(AuthenticationResource.class);
	
	@Autowired
	private AuthenticationService authentication;
	
	@RequestMapping(value = "/authentication", method = RequestMethod.POST)
	public @ResponseBody User authentication(@RequestBody User user) throws ServletException{
		return authentication.authenticate(user);
	}
}
