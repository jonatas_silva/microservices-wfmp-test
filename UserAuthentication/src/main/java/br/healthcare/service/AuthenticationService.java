package br.healthcare.service;

import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.servlet.ServletException;

import org.springframework.stereotype.Service;

import br.healthcare.model.Key;
import br.healthcare.model.User;

@Service
public class AuthenticationService {

	public User authenticate(User user) throws ServletException {
		if (user.getLogin() == null || user.getPassword() == null)
			throw new ServletException("Name and password are required.");

		User userAuthenticate = validate(user.getLogin(), user.getPassword(), user.getProfile());

		if (userAuthenticate != null) {// TOKEN Generate by JWT API
			Key key = new Key();
			System.out.println(user.toString());
			String value = key.getKeyByProfile(userAuthenticate.getProfile().intValue());
			
			String token = Jwts.builder()
					.signWith(SignatureAlgorithm.HS256, value)
					.setExpiration(new Date(System.currentTimeMillis() + (4*60*60*1000))) //four hours
					.compact();
			
			userAuthenticate.setAccessToken(token);
			return userAuthenticate;	
		}
		throw new ServletException("User not found.");
	}

	private User validate(String login, String password, Long profile){
		User user = new User(login, password);
		user.setProfile(profile);
		return user;
	}

}
