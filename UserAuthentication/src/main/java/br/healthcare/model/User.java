package br.healthcare.model;

public class User {
	
	private String login;
	private String password;
	private Long profile;
	private String accessToken;
	
	public User() {}
	
	public User(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Long getProfile() {
		return profile;
	}
	
	public void setProfile(Long profile) {
		this.profile = profile;
	}
	
	public String getAccessToken() {
		return accessToken;
	}
	
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@Override
	public String toString() {
		return "User [login=" + login + ", password=" + password + ", profile="
				+ profile + ", accessToken=" + accessToken + "]";
	}
}