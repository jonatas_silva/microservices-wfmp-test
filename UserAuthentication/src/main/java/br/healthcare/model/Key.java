package br.healthcare.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Key {
	
	/*
	 * 
	 * Keys used by Json Web Token of according with user profile
	 * 
	 */
	
	private static final String HEALTH_PROFESSIONAL = "key1-dfdsf344r34";
	
	private static final String PATIENT = "key2-75665gdsdf3s";
	
	private static final String ADMIN = "key3-frf8764bkpo";
	
	public static String getKeyByProfile(int profile){
		
		switch(profile){
			case 1:
				return HEALTH_PROFESSIONAL;
			case 2:
				return PATIENT;
			case 3:
				return ADMIN;
			default:
				return null;
		}
	}
	
	/*
	 * 
	 * Filter: <'method:service'><'List profiles'>
	 * 
	 */
	
	private static final HashMap<String, List<String>> filter = new HashMap<String, List<String>>() {
		
		private static final long serialVersionUID = 1L;

	{
		put("POST:workflow/register", 	new ArrayList<>(Arrays.asList(HEALTH_PROFESSIONAL)));
		put("POST:workflow/run", 		new ArrayList<>(Arrays.asList(HEALTH_PROFESSIONAL, PATIENT)));
		
		put("GET:microservice",		new ArrayList<>(Arrays.asList(ADMIN)));
		put("POST:microservice", 	new ArrayList<>(Arrays.asList(ADMIN)));
		put("PUT:microservice", 	new ArrayList<>(Arrays.asList(ADMIN)));
		put("DELETE:microservice", 	new ArrayList<>(Arrays.asList()));
	}};
	
	public static boolean isAllowed(String method, String service, String key){
		if(key.equals(ADMIN)) return true;
		
		List<String> profiles = filter.get(method+":"+service);
		
		for (String value : profiles) {
			if(value.equals(key))
				return true;
		}
		return false;
	}
}