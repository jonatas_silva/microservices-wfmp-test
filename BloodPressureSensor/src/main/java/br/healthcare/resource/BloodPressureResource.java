package br.healthcare.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.healthcare.model.BloodPressure;
import br.healthcare.service.BloodPressureService;

@RestController
@RequestMapping("/healthcare/bloodpressure")
public class BloodPressureResource {
	
	private static final Logger log = LoggerFactory.getLogger(BloodPressureResource.class);

	@Autowired
	private BloodPressureService service;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public BloodPressure getBloodPressure() throws Exception{
		return service.getBloodPressure();
	}
}