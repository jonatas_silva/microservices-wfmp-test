package br.healthcare.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.healthcare.model.BloodPressure;

@Service
public class BloodPressureService {
	
	private static final Logger log = LoggerFactory.getLogger(BloodPressureService.class);
	
	public BloodPressure getBloodPressure() {
		return new BloodPressure(135, 90, "90");
	}
}
