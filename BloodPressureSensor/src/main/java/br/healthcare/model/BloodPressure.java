package br.healthcare.model;

import java.io.Serializable;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan
public class BloodPressure implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int systolic;
    private int diastolic;
    private float mean;
    private String pulse;
    
    public BloodPressure() {}
    
	public BloodPressure(Integer systolic, Integer diastolic) {
		super();
		this.systolic = systolic;
		this.diastolic = diastolic;
	}
	
	public BloodPressure(Integer systolic, Integer diastolic, String pulse) {
		super();
		this.systolic = systolic;
		this.diastolic = diastolic;
		this.pulse = pulse;
	}
	
	public Integer getSystolic() {
		return systolic;
	}
	public void setSystolic(Integer systolic) {
		this.systolic = systolic;
	}
	public Integer getDiastolic() {
		return diastolic;
	}
	public void setDiastolic(Integer diastolic) {
		this.diastolic = diastolic;
	}
	public float getMean() {
		return mean;
	}
	public void setMean(float mean) {
		this.mean = mean;
	}
	public String getPulse() {
		return pulse;
	}
	public void setPulse(String pulse) {
		this.pulse = pulse;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
