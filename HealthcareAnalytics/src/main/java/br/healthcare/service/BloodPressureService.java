package br.healthcare.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.healthcare.model.BloodPressure;
import br.healthcare.model.HealthAssessment;

@Service
public class BloodPressureService {
	
	private static final Logger log = LoggerFactory.getLogger(BloodPressureService.class);
	
	public static List<BloodPressure> bloodPressureValues = new ArrayList<>();

	public BloodPressure getBloodPressure(long id, long user) {
		return null;
	}

	public HealthAssessment bloodPressureAnalyze(BloodPressure bloodPressure) {
		
		// read the other values collected
		//List<BloodPressure> bloodPressureValues = readBloodPressureValues();
		
		return createHealthAssessment(bloodPressureValues, bloodPressure);
	}
	
	private HealthAssessment createHealthAssessment(List<BloodPressure> bloodPressureValues, BloodPressure newBp){
		HealthAssessment ha = new HealthAssessment();
		
		float systolicSum = 0;
	    float diastolicSum = 0;
	    
	    bloodPressureValues.add(newBp);
		
		for (BloodPressure bloodPressure : bloodPressureValues) {
			systolicSum += bloodPressure.getSystolic();
			diastolicSum += bloodPressure.getDiastolic();
		}
		
		ha.setMean24hoursSystolic(systolicSum/bloodPressureValues.size());
		ha.setMean24hoursDiastolic(diastolicSum/bloodPressureValues.size());
		
		if(bloodPressureValues.size()>=3){
			ha.setDiagnosis(true);
			
			if(ha.getMean24hoursSystolic()>=135 && ha.getMean24hoursDiastolic()>=85)
				ha.setHypertension(true);
			
			bloodPressureValues.clear();
		}
		return ha;
	}
}
