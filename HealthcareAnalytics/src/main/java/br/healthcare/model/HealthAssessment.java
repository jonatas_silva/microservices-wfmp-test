package br.healthcare.model;

import java.io.Serializable;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan
public class HealthAssessment implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private float mean24hoursSystolic;
	private float mean24hoursDiastolic;
    private boolean hypertension;
    private boolean diagnosis;
    
    public HealthAssessment() {}
    
	public float getMean24hoursSystolic() {
		return mean24hoursSystolic;
	}
	public void setMean24hoursSystolic(float mean24hoursSystolic) {
		this.mean24hoursSystolic = mean24hoursSystolic;
	}
	public float getMean24hoursDiastolic() {
		return mean24hoursDiastolic;
	}
	public void setMean24hoursDiastolic(float mean24hoursDiastolic) {
		this.mean24hoursDiastolic = mean24hoursDiastolic;
	}
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(boolean diagnosis) {
		this.diagnosis = diagnosis;
	}
}
