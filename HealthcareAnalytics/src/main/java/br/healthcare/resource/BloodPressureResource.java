package br.healthcare.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.healthcare.model.BloodPressure;
import br.healthcare.model.HealthAssessment;
import br.healthcare.service.BloodPressureService;

@RestController
@RequestMapping("/healthcare/analyze")
public class BloodPressureResource {
	
	private static final Logger log = LoggerFactory.getLogger(BloodPressureResource.class);

	@Autowired
	private BloodPressureService service;
	
	@RequestMapping(value = "/{id}/{user}", method = RequestMethod.GET)
	public BloodPressure getEnsaio(@PathVariable("id") long id, @PathVariable("user") long user) throws Exception{
		return service.getBloodPressure(id, user);
	}
	
	@RequestMapping(value = "/bloodpressure", method = RequestMethod.POST)
	public @ResponseBody HealthAssessment bloodPressureAnalyze(@RequestBody BloodPressure bloodPressure){
		return service.bloodPressureAnalyze(bloodPressure);
	}
}