package br.healthcare.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.healthcare.model.HealthAssessment;
import br.healthcare.model.HealthNotification;
import br.healthcare.model.MedicalReport;
import br.healthcare.service.NotificationService;

@RestController
@RequestMapping("/healthcare/professional")
public class HealthcareResource {
	
	private static final Logger log = LoggerFactory.getLogger(HealthcareResource.class);

	@Autowired
	private NotificationService service;
	
	@RequestMapping(value = "/notify", method = RequestMethod.POST)
	public @ResponseBody HealthNotification notify(@RequestBody HealthAssessment healthAssessment){
		return service.notifyProfessional(healthAssessment);
	}
	
	@RequestMapping(value = "/evaluation", method = RequestMethod.POST)
	public @ResponseBody MedicalReport evaluation(@RequestBody HealthAssessment healthAssessment){
		MedicalReport report = new MedicalReport();
		//if(healthAssessment.isDiagnosis()){
			report.setId(21212);
			report.setTitle("Avaliação realizada pelo médico X");
			report.setDescription("Descrição da avaliação realizada pelo médico X.");
		//}
		return report;
	}
}