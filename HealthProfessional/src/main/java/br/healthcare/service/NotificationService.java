package br.healthcare.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.healthcare.model.HealthAssessment;
import br.healthcare.model.HealthNotification;

@Service
public class NotificationService {
	
	private static final Logger log = LoggerFactory.getLogger(NotificationService.class);

	public HealthNotification notifyProfessional(HealthAssessment healthAssessment) {
		HealthNotification healthNotification = new HealthNotification();
		healthNotification.setEvaluated(false);
		healthNotification.setObservation("Blood Pressure Valeu: "+healthAssessment.getMean24hoursSystolic()+"/"+healthAssessment.getMean24hoursDiastolic());
		return healthNotification;
	}
}
