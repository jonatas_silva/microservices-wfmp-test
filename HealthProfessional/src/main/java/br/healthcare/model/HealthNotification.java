package br.healthcare.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan
public class HealthNotification {
	
	private static final long serialVersionUID = 1L;
	
	private boolean evaluated;
	private String observation;
	
	public HealthNotification() {}

	public boolean isEvaluated() {
		return evaluated;
	}

	public void setEvaluated(boolean evaluated) {
		this.evaluated = evaluated;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}
}
