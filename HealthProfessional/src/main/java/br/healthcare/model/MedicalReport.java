package br.healthcare.model;

import java.io.Serializable;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan
public class MedicalReport implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String title;
	private String description;
	
	public MedicalReport() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "MedicalReport [id=" + id + ", title=" + title
				+ ", description=" + description + "]";
	}
}
